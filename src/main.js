import './assets/main.css';

import { createApp } from 'vue';
import App from './App.vue';

import 'ace-builds/src-noconflict/mode-json';
import 'ace-builds/src-noconflict/mode-xml';
import 'ace-builds/src-noconflict/theme-chrome';
import 'ace-builds/src-noconflict/theme-dawn';

createApp(App).mount('#app');
