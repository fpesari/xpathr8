export const XPathService = {
  apply(xmlText, xpathQuery) {
    let xmlTextClean = xmlText.trim();
    let xpathQueryClean = xpathQuery.trim();
    if (xmlTextClean === '' || xpathQueryClean === '') return '';
    let xmlRootNode;
    try {
      xmlRootNode = new DOMParser().parseFromString(xmlTextClean, 'text/xml');
    } catch {
      return '';
    }
    let foundNodes;
    try {
      foundNodes = xmlRootNode.evaluate(
        xpathQueryClean.trim().toString(),
        xmlRootNode,
        null,
        XPathResult.ANY_TYPE,
        null
      );
    } catch {
      return '';
    }
    // Thanks https://developer.mozilla.org/en-US/docs/Web/API/Document/evaluate
    var text = '';
    var result = foundNodes.iterateNext();
    while (result) {
      let content = result.textContent;
      if (content) {
        text += `${content.trim()}\n`;
      }
      result = foundNodes.iterateNext();
    }
    let output = JSON.stringify(
      text.split('\n').flatMap((e) => (e ? [e.trim()] : [])),
      null,
      2
    );
    return output;
  }
};
