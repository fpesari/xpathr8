# xpathr8

xpathr8 is a simple XPath-based data extractor for XML files.

It extracts the results of an XPath query to a JSON array, ready for further
processing.

It was made both as a utility and a way to learn VueJS.

## Screenshot

![Screenshot of xpathr8](screenshot.png)

## License

All files in this repository are free/libre software (FLOSS) and, except where
otherwise noted, are released under the terms of the GNU Affero General Public
License, version 3.0 or any later version.

